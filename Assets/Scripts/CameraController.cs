﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public class CameraController : MonoBehaviour {

    bool isDisabled = false;
    public float verticalSensibility = 10.0f;

    public float minimumVertical = -45.0f;
    public float maximumVertical = 45.0f;

    public float rotationX = 0.0f;

    ClientUDP clientUDP;
    private float prevAzimuth = -1000000;

    void Start()
    {
        clientUDP = GameObject.Find("UDPListener").GetComponent<ClientUDP>();
    }

    void Update()
    {
        if (!isDisabled)
        {
            //rotationX -= Input.GetAxis("Mouse Y") * verticalSensibility;
           // rotationX = Mathf.Clamp(rotationX, minimumVertical, maximumVertical);

            //transform.localEulerAngles = new Vector3(rotationX, transform.localEulerAngles.y, 0.0f);
            /*
            if (prevAzimuth >= -180)
            {
                rotationX = clientUDP.azimuth - prevAzimuth;
            } else 
            {
                rotationX = 0;
            }

            prevAzimuth = clientUDP.azimuth;
            transform.localEulerAngles = new Vector3(0.0f, rotationX, 0.0f);
            Debug.Log("rotation: " + rotationX);
            */
        }
       
    }

    public void ChangeState()
    {
        isDisabled = !isDisabled;
    }
}
