﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public class PlayerController : MonoBehaviour
{

    bool isDisabled = false;

    private Rigidbody playerRigidbody;
    public float movementSmoothing = 0.2f;
    public float rotationSmoothing = 2f;

    private float pozyxScaleModifier = 200.0f;

    GameObject scoreManager;
    private int score = 0;

    ClientUDP clientUDP;

    const float extremelySmallValue = -999999999.0f;
    float prevAzimuth = extremelySmallValue; // bardzo mało
    float smoothedAzimuth = extremelySmallValue; // bardzo mało

    float smoothedPositionX = extremelySmallValue;
    float smoothedPositionY = extremelySmallValue;
    public float movementLerpModifier = 3.0f;

    void Start()
    {
        scoreManager = GameObject.Find("ScoreManager");
        playerRigidbody = GetComponent<Rigidbody>();
        clientUDP = GameObject.Find("UDPListener").GetComponent<ClientUDP>();

    }

    private void Update() {}

    void FixedUpdate()
    {
        if (!isDisabled)
        {
            if (Mathf.Abs(clientUDP.positionX - smoothedPositionX) < 300.0f)
            {
                smoothedPositionX = (1 - movementSmoothing) * smoothedPositionX + movementSmoothing * clientUDP.positionX; // IIR filter
            }
            else
            {
                // przeskocz do nowej wartości
                smoothedPositionX = clientUDP.positionX;
            }

            if (Mathf.Abs(clientUDP.positionY - smoothedPositionY) < 300.0f)
            {
                smoothedPositionY = (1 - movementSmoothing) * smoothedPositionY + movementSmoothing * clientUDP.positionY; // IIR filter
            }
            else
            {
                // przeskocz do nowej wartości
                smoothedPositionY = clientUDP.positionY;
            }

            Vector3 newPosition = new Vector3(smoothedPositionX / pozyxScaleModifier, 1.0f, smoothedPositionY / pozyxScaleModifier);

            //transform.position = newPosition;
            transform.position = Vector3.Lerp(transform.position, newPosition, Time.deltaTime * movementLerpModifier);
            //Debug.Log("Pozyx position - (" + smoothedPositionX + "," + smoothedPositionY + ")");
            //Debug.Log("Position - (" + transform.position.x + "," + transform.position.z + ")");

            if (prevAzimuth >= -180)
            {
                if (((smoothedAzimuth > 0) != (clientUDP.azimuth > 0) && // zmienia znak
                     (Mathf.Abs(smoothedAzimuth) > 150)))               // i jest skierowany mniej więcej na południe 
                {
                    // jeżeli zmiana znaku  w okoliczy 180/-180
                    if (clientUDP.azimuth > 0)
                    {
                        smoothedAzimuth += 360;
                    } else
                    {
                        smoothedAzimuth -= 360;
                    }

                }
                float alfa = 0.05f;
                if (Mathf.Abs(clientUDP.azimuth - smoothedAzimuth) < 20f)
                {
                    smoothedAzimuth = (1 - alfa) * smoothedAzimuth + alfa * clientUDP.azimuth; // IIR filter
                }
                else
                {
                    // przeskocz do nowej wartości
                    smoothedAzimuth = clientUDP.azimuth;
                }
            }
            else
            {
                smoothedAzimuth = clientUDP.azimuth;
            }

            prevAzimuth = clientUDP.azimuth;
            //rotationX = clientUDP.azimuth;

            //transform.localEulerAngles = new Vector3(0.0f, smoothedAzimuth, 0.0f);
            //Debug.Log("rotation: " + smoothedAzimuth);

            Vector3 newAngle = new Vector3(0.0f, smoothedAzimuth + 90.0f, 0.0f);

            Quaternion desiredQuat = Quaternion.Euler(newAngle);
            transform.rotation = Quaternion.Lerp(transform.rotation, desiredQuat, Time.deltaTime * rotationSmoothing);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Coin"))
        {
            scoreManager.GetComponent<ScoreController>().IncreaseScore();
            Destroy(other.gameObject);
        }
    }

    public void ChangeState()
    {
        isDisabled = !isDisabled;
    }
}
