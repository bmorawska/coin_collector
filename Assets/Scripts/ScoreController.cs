﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour {

    int numberOfCoins;
    int currentScore;

    public Text currentScoreUI;

    // game over panel
    public Text gameOverScoreUI;
    public Text remainingCoinsUI;

    // win panel
    public Text winScoreUI;
    public Text leftTimeUI;
    public Text totalScoreUI;

    public RectTransform scorePanel;


	void Start () {
        numberOfCoins = GameObject.FindGameObjectsWithTag("Coin").Length;
        currentScore = 0;
        UpdateScoreUI();
	}
	
	public void IncreaseScore() {
        currentScore++;
        UpdateScoreUI();
	}

    void UpdateScoreUI()
    {
        currentScoreUI.text = currentScore.ToString();
    }

    public bool AllCoinsGathered()
    {
        if (currentScore == numberOfCoins)
            return true;
        else
            return false;
    }

    public void ShowWinScore(float leftTime)
    {
        scorePanel.gameObject.SetActive(false);
        winScoreUI.text = currentScore.ToString();
        leftTimeUI.text = leftTime.ToString("F1");
        totalScoreUI.text = ((float)currentScore + leftTime).ToString("F1");
    }

    public void ShowGameOverScore()
    {
        scorePanel.gameObject.SetActive(false);
        gameOverScoreUI.text = currentScore.ToString();
        remainingCoinsUI.text = (numberOfCoins - currentScore).ToString();
    }

    public void Restart()
    {
        scorePanel.gameObject.SetActive(true);
        numberOfCoins = GameObject.FindGameObjectsWithTag("Coin").Length;
        currentScore = 0;
        UpdateScoreUI();
    }
}
